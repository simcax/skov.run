---
title: "Tisvilde Ultra 2022"
date: 2022-10-13T07:22:18+02:00
draft: false
coverImage: '/images/tisvilde-ultra-2022/IMG20221002072943.jpg'
autoThumbnailImage: true
#thumbnailImage: 'images/julsoe-ultra-2022/FB_IMG_1651341712384.jpg'
categories:
- races
- running
- ultra
tags:
- ultra
- race briefing
metaAlignment: center
---
# The race which disappeard
A year ago a fellow runner from Lejre persuaded me and some others to participate in [Tisvilde Ultra](https://active-signup.com/runs/tisvilde-ultra-trail-2022/). I signed up for the 50 km distance. 

I hadn't really gotten the training in, I wanted for such a race, but wanted to throw all I had in it. So on the early morning hours of Saturday October 2nd I set out for Tisvilde

![](/images/tisvilde-ultra-2022/maps_tisvilde_ultra.png)

The drive was about an hour from where I live, and I might have been a bit late - so I arrived with about 30 minutes to race start, and found this

![](/images/tisvilde-ultra-2022/IMG_20221002_065721_088.jpg)

An empty parking lot in the middle of the forest. No race start, no bip stand - just nothing. I double checked the dates, and everything checked out. I tried driving a bit further into the woods, and came across another car - they were looking for race start as well, and had an idea about another place where the race start could be. So I decided to follow them, but before I got my car backed up and turned around they were gone. 

I drove back to the highway, and tried a bit further west, into the woods again, but no luck. 

By this time it was 10 minutes to race start, and driving around looking for the start didn't really suit my mood. I remember coming across a sign signaling the race direction in the woods, and decided to park my car near that, and just run the course by myself without a bip. 
I might as well get a run in, since I was in the woods anyway. 

I parked, changed and found the sign, and it was only 2-3 minutes past race start time. Pretty okay with the situation I started running, and knew it would be a long day on the trail. My body didn't feel all together fantastic, and the below par race start had set something negatively in my mind. 

The trail however was really nice. A lot of single trail in the deep woods, not another human being in sight. 15 minutes into the run, another runner caught up to me. As it turns out I had started only a couple of kilometers from start.

He was quite surprised I was there - he was one of 50 miles runners, and he was in front he said. So where did I come from?!! I told him my story, and he wasn't surprised. The guys who were bringing the timer equipment as well as the guy with the bips were both late. And so the 50 milers started a whole half hour late. That explained how a 50 miler wasn't further along the route. I was supposed to run 50 kilometers, and the start was a full hour later than the 50 milers. The guys was running quite a bit faster than me, so the company was shortlived, and I found myself alone on the trails again.

![](/images/tisvilde-ultra-2022/IMG20221002072027.jpg)

As always I ran in sandals, and this time it was in a pair of prototypes, which was for allround use. Not specifically for trail - that meant the very few places the ground was slippery, it was a bit more slippery in these ones, than what I am used to. The sandals did quite okay though, and being allround and with quite the soft footbed, lend a nice landing and my feet were happy. My groin however was not. I have had issues with it before, and during this race it acted up quite a bit, getting sore. I tried my best to get into focusing on technique, and held it at bay until about the 15 kilometer mark. By now I was tired, seems 20-30 km distance per week wasn't quite enough for me to be ready for this. I also hadn't really been that much in the woods, but mostly training on gravel and asphalt for transportation. 

![](/images/tisvilde-ultra-2022/IMG20221002084102.jpg)

As I came upon the beach and we had to run along it, I got the first taste of missing marking on the route. I obviously hadn't been at the race briefing, and thus didn't have the information we could just run along the water until a marking led us back up into the forest. That took me quite a while to figure out, as I spotted a runner by the waterline. At this time I was tired, and knew I wouldn't make the 2 rounds, I had signed up for. I would cut it at one round. My groin was really complaining and I couldn't seem to get around the pain. 

![](/images/tisvilde-ultra-2022/IMG20221002093316.jpg)

I found the marking to go into the forest again, and after some up and down of stairs, as well as some nice single trail passages, we came out for a long piece by the shore once again. I walked a lot of this, as the sand was making the pain worse. I sat down in squat several times to see if I could stretch out my hip flexors, which seems to be the problem area, but it didn't go away. 
I finally came to the point where we were led inland again, and soon after that came upon the start area! I passed the race director, who called me out and was confused, I didn't have a bip on my chest?! I explained it as it was, I couldn't find the start area, and so just decided to run anyway. 

![](/images/tisvilde-ultra-2022/IMG20221002102126-EFFECTS.jpg)

I ran the last couple of kilometers to my car, and it took me 10 minutes to change out of my running clothes and into nice warm dry clothes due to the pain. I dreaded the next couple of days, and not least getting out of the car again. 
Luckily I was okay by the next day, and only mildly sore. The pain disappeared on day 2, and I was happy to have missed a potential injury. 
I only got 27 km on that race, and not the 50km I had hoped for. But lacking training, bad form, stress on the race day, made for a bad cocktail. I am just happy I did not leave the race with an injury.
All in all a not too good experience, but I got to run on some nice trails and get a fantastic nature experience. 

![](/images/tisvilde-ultra-2022/strava5984928151990928194.jpg)