---
title: "Julsoe Ultra 2022"
date: 2022-08-27T16:33:29+02:00
draft: false
coverImage: 'images/julsoe-ultra-2022/FB_IMG_1651341712384.jpg'
autoThumbnailImage: true
thumbnailImage: 'images/julsoe-ultra-2022/FB_IMG_1651341712384.jpg'
categories:
- races
- running
- ultra
tags:
- ultra
- race briefing
metaAlignment: center
---
# 
My first race of the year was Julsø Ultra 50 miles. I have not before tried that distance on a non-stop single loop route. 

I have previously run 100km in the backyard ultru format, where you run a loop of 6.7km ever hour.

So I was quite nervous about the project. On top of it being my first attempt, my training had really not gone well for the past 6 months.

Twice before I've run Julsø Ultra. In 2017 I ran the 30km route, and the year after the 60km route. Both of which were some 5km longer than the name of the route lead me to believe ;-) 

# The route
The route was first a loop east of Silkeborg of about 20km. After that loop the route continued on the south side of the lake Julsø (actually Christmas Lake cleanly translated).
![](/images/julsoe-ultra-2022/strava-route.png) 

As you might guess, the route continued on the other side of the lake and back to start. Unfortunately I did not get that far. 


# Sandals on the day
As the weather was a bit uncertain, I had decided to run in a pair of beta sandals, which I was testing out. They are called mega grip and you might understand why from the rough pattern underneath the sole

![](/images/julsoe-ultra-2022/panta-mega-grip.jpg)

Prior to the race I had run around 100km in them, and had been very satisfied with them. They feel great under my feet and sit so tight without my foot scooting around even when the rain is falling heavily. The triangles underneath lends a fantastic grip on muddy slippery trails, and I must admit, I had somewhat hoped for a bit of rain to really test them out. 

# The Race
The race was set to start at 7:00 in the morning, and it was a bit chilly, but quite nice weather. 

![](/images/julsoe-ultra-2022/race-start.jpg)*Anticipating runners*

As we started out the first 15km was without issues. From there on it got real thoguh. I really tried to keep the pace AND pulse down, enjoying the scenery. Running when it was flat, walking up the hills and racing down hill. I just somehow couldn't seem to keep my pulse down, no matter what I did. 

In hindsight I should probably have walked a bit more - lesson learned I hope. First sort of trouble was at a point where I had been running behind a few guys, somewhat following them. All of a sudden they were gone, and I was running all by myself. The flags indicating the route had turned pink from the orange they had been the whole way. The wood turned from open to a closed pine forest, and the trail was down hill - quite steep. I stopped. *"This must be wrong..."* I remember thinking. 

![](/images/julsoe-ultra-2022/pine-forest.jpg)*pink flags are not good!*

So I ran up the hill again, and some of the other runners came running towards me. We agreed to keep running. After running down and down of quite rough terrain, the woods opened and a path ran across the direction we ran. As we are running out on the path and start following the orange flags, some other runners came from the back, and it dawned on us - we had been off route. Not a good thing for us, since the terrain was hard to get through. It turned out the difference was 300 meters, so we agreed it was quite okay. 

This was when my troubles started for real. My back thighs threatened with cramps, and I decided to walk for a while. It worked and I kept running. The end of the loop around the lake was coming close, and se was the first 20km. Just before the loop ended I was taken by surprise, and out of nowhere an old school buddy - Peter - stood there! 

![](/images/julsoe-ultra-2022/meeting-peter.jpg)

We had a short chat, before I was on my way again. It was so cool to meet him again. He lived just around the corner, had been following my Garmin live tracker, which I had posted on Facebook. Quite the awesome surprise. It also helped me to forget my protesting back thighs. 

The loop ended, and the trail continued on the south side of the lake. The trails were open and wide for a long while, eventually opening up towards the lake with a nice view of the water. 

I was counting the kilometers by chunks of 5. 25...30...35. I was now really in trouble, the cramps pushing, but never materializing. I needed to walk for a while, and I had my mind steeled on at least getting a marathon in the bag. As I was walking there, I saw a fellow runner sitting on a stub, fixing his shoes. As it turned out, it was a guy I had chatted with once in a while on Instagram - australian I believe. His instagram tag is [@jpneumas](https://www.instagram.com/jpneumas/) - it was cool to meet up, and we had a fantasic chat walking together for a while. As we were walking together we eventually got up to the top of "Himmelbjerget" (Mountain of Heaven - yeah we danes are presumptious). I must admit, I am terrible at remembering names, although I know we traded up names on the run - sorry about that if you are reading this. 

![jpneumas](/images/julsoe-ultra-2022/meeting-jpneumas.jpg)

![on-the-top](/images/julsoe-ultra-2022/to-the-top.jpg)

After reaching the top, my legs felt better, and we parted ways, as he was not quite ready to run yet. So I made my way onwards. Now my feet joined the screaming match. They hurt. So I was coming up on 45 km. The third aid station would be coming up during the next 10km, where my drop bag was. I was now going back and forth between running and walking. A couple of kilometers before the aid station I was now only walking, my feet on fire, my thighs complaining loudly. Should I continue? 

As I came upon the aid station, I felt the last energy leave my body and all the aches come rushing in. I was done...

![](/images/julsoe-ultra-2022/the-end.jpg)

I ended up throwing in the towel, and as I sat on the grass, taking it all in, I was happy to have reached yet another Ultra distance. Even if it turned out to be a DNF. Well, sitting writing this article almost 4 months later, I can feel I am ready again, hungry. For the joy of trails, for the endurance game, for the jorney, and the pain. 

Next challenge will be [Tisvilde Ultra](https://active-signup.com/runs/tisvilde-ultra-trail-2022/) on October 2nd. I am looking forward to it, while I am dreading it. I have almost had no training the past 4 months. Only training speed for running the [DHL Relay Race](https://sparta.dk/dhl-stafetten/)
Not really been doing too well on that front either, and now a week before the race, I got Covid. After 3 days I am well again, and hope to make a somewhat good effort. 

Anyway, trail training is up after the DHL Relay Race, so let the training begin!

