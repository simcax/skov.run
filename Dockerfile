FROM nginx:alpine
LABEL maintainer="carsten@skov.codes"
COPY public /usr/share/nginx/html

WORKDIR /usr/share/nginx/html
